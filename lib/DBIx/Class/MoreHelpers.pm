package DBIx::Class::MoreHelpers;
# VERSION
# AUTHORITY
# ABSTRACT: More helpers for DBIx::Class
use strict;
use warnings;

1;

__END__

=pod

=head1 SYNOPSIS

More tools for simplifying common uses of L<DBIx::Class>

=head1 DESCRIPTION

Take a look at the individual components of this bundle for more documentation:

=over 2

=item * L<DBIx::Class::Helper::ResultSet::MoreShortcuts>

=back

=head1 DEPENDENCIES

=over 2

=item * L<DBIx::Class>

=item * L<Dbix::Class::Helpers>

=back

=head1 ACKNOWLEDGEMENTS

None of this could have happened without L<fREW|https://metacpan.org/author/FREW>'s outstanding
work on L<DBIx::Class::Helpers>, on which this module depends.

Also, big thanks to my teammates at Clearbuilt for batting around ideas and encouragement.

=cut
