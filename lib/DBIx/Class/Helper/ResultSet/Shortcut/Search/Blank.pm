package DBIx::Class::Helper::ResultSet::Shortcut::Search::Blank;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 blank(@columns || \@columns)

 $rs->blank('status');
 $rs->blank(['status', 'title']);

=cut

sub blank {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '=' => '' }, @columns );
}

1;
