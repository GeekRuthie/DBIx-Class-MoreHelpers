package DBIx::Class::Helper::ResultSet::Shortcut::Search::Nonzero;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 nonzero(@columns || \@columns)

 $rs->nonzero('active');
 $rs->nonzero(['active', 'blocked']);

=cut

sub nonzero {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '!=' => 0 }, @columns );
}

1;
