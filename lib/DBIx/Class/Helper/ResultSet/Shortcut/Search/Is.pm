package DBIx::Class::Helper::ResultSet::Shortcut::Search::Is;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 is(@columns || \@columns)

 $rs->is('active');
 $rs->is(['active', 'blocked']);

=cut

sub is {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '=' => 'true' }, @columns );
}

1;
