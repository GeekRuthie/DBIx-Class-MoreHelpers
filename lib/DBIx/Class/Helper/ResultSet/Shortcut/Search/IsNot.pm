package DBIx::Class::Helper::ResultSet::Shortcut::Search::IsNot;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 is_not(@columns || \@columns)

 $rs->is_not('active');
 $rs->is_not(['active', 'blocked']);

=cut

sub is_not {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '=' => 'false' }, @columns );
}

1;
