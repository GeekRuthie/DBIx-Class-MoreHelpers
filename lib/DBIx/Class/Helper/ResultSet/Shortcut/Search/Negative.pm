package DBIx::Class::Helper::ResultSet::Shortcut::Search::Negative;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 negative(@columns || \@columns)

 $rs->negative('active');
 $rs->negative(['active', 'blocked']);

=cut

sub negative {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '<' => 0 }, @columns );
}

1;
