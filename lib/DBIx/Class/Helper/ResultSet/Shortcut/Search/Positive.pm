package DBIx::Class::Helper::ResultSet::Shortcut::Search::Positive;
use strict;
use warnings;
use parent 'DBIx::Class::Helper::ResultSet::Shortcut::Search::Base';

=head2 positive(@columns || \@columns)

 $rs->positive('active');
 $rs->positive(['active', 'blocked']);

=cut

sub positive {
   my ( $self, @columns ) = @_;

   return $self->_helper_apply_search( { '>' => 0 }, @columns );
}

1;
